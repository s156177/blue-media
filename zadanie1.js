describe('zadanie 1', () => {
// sekcję przechowywania zawartości localStorage wrzuciłabym np. do pliku support/commands.js
// aby nie zapisywać tego w każdym teście
// poniższe rozwiązanie jest tymczasowe i nie wymaga wysyłania dodatkowych plików

  let LOCAL_STORAGE_MEMORY = {};

  Cypress.Commands.add("saveLocalStorage", () => {
    Object.keys(localStorage).forEach(key => {
      LOCAL_STORAGE_MEMORY[key] = localStorage[key];
    });
  });

  Cypress.Commands.add("restoreLocalStorage", () => {
    Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
      localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
    });
  });


  beforeEach(() => {
    cy.restoreLocalStorage()
    cy.visit('https://app.bluemedia.pl/logowanie')
  })

  afterEach(() => {
    cy.saveLocalStorage()
  })

  it('logowanie', () => {

    cy.get('#email').type('rekrutacja-cypress1@bm.pl', {timeout: 10})
    cy.get('button').click()
    cy.get('#password').type('Qwerty12')
    cy.get('button').click()
  })

  it('sprawdzenie czy pojawiają się ikonki operatorów (Vectra, UPC, INNOGY, PGE) i sprawdzenie, czy adresy url, podpięte pod odsyłacz \'Przejdź do ebok’ są zgodne z ikonkami', () => {
    let basePath='.no-payments-info-box > ul:nth-child(2)'
    cy.get(basePath + ' > li:nth-child(1) > a:nth-child(1)').should('have.prop', 'href')
        .and('equal', 'https://ebok.vectra.pl/#/login')
    cy.get(basePath + ' > li:nth-child(2) > a:nth-child(1)').should('have.prop', 'href')
        .and('equal', 'https://www.upc.pl/sign-in/')
    cy.get(basePath + ' > li:nth-child(3) > a:nth-child(1)').should('have.prop', 'href')
        .and('equal', 'https://moje.innogy.pl/Logowanie')
    cy.get(basePath + ' > li:nth-child(4) > a:nth-child(1)').should('have.prop', 'href')
        .and('equal', 'https://ebok.gkpge.pl/ebok/faces/profil/logowanie.xhtml')
  })

  it('wylogowanie', () => {
    cy.get('a.logout:nth-child(5)').click()
    cy.get('.alert').should('have.text', 'Nastąpiło poprawne wylogowanie. Możesz teraz bezpiecznie zamknąć okno lub zaloguj się ponownie.')
  })
})