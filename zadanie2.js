describe('zadanie 2', () => {

  let LOCAL_STORAGE_MEMORY = {};

  Cypress.Commands.add("saveLocalStorage", () => {
    Object.keys(localStorage).forEach(key => {
      LOCAL_STORAGE_MEMORY[key] = localStorage[key];
    });
  });

  Cypress.Commands.add("restoreLocalStorage", () => {
    Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
      localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
    });
  });

  beforeEach(() => {
    cy.restoreLocalStorage()
    cy.visit('https://app.bluemedia.pl/logowanie')
  })

  afterEach(() => {
    cy.saveLocalStorage()
  })

  it('logowanie', () => {
    cy.get('#email').type('rekrutacja-cypress1@bm.pl{enter}')

    cy.get('#password').type('Qwerty12{enter}')
  })

  it('sprawdź czy na górnej belce dostępne są wszystkie 4 zakładki', () => {
    cy.contains('Opłaty automatyczne').click()
    cy.get('.heading > span:nth-child(1)').should('contain', 'Nie regulujesz żadnych opłat automatycznie')
    const pages = ['Lista transakcji', 'Formy płatności', 'Ustawienia i dane konta']
    pages.forEach(page => {
      cy.contains(page).click()
      cy.get('.heading').should('contain', page)
    })
  })

  it('wylogowanie', () => {
    cy.get('a.logout:nth-child(5)').click()
    cy.get('.alert').should('have.text', 'Nastąpiło poprawne wylogowanie. Możesz teraz bezpiecznie zamknąć okno lub zaloguj się ponownie.')
  })

})
